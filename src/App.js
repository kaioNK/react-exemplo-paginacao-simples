import React, {Component} from 'react'
import './App.css'

export default class App extends Component{
  state = {
    todos: ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p'],
    currentPage: 1,
    todosPerPage: 5
  }

  handleClick = (e) => {
    this.setState({
      currentPage: Number(e.target.id)
    })
  }

  render(){
    const {todos, currentPage, todosPerPage} = this.state

    const indexOfLastTodo = currentPage * todosPerPage
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage
    const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo)

    const renderTodos = currentTodos.map((todo, index) => {
      return <li key={index}>{todo}</li>
    })
    
    const pageNumbers = []
    for(let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++){
      pageNumbers.push(i)
    }
    const renderPageNumbers = pageNumbers.map(number => {
      return(
        <li
          key={number}
          id={number}
          onClick={this.handleClick}
        >
          {number}
        </li>
      )
    })

    return(
      <div>
        <ul>
          {renderTodos}
        </ul>
        <ul id="page-numbers">
          {renderPageNumbers}
        </ul>
      </div>
    )
  }
}